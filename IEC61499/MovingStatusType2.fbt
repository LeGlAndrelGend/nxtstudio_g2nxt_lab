<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE FBType SYSTEM "../LibraryElement.dtd">
<FBType Name="MovingStatusType2" Comment="Protection layer" Namespace="Main">
  <Identification Standard="61499-2" />
  <VersionInfo Organization="The University of Auckland" Version="1.0" Author="Sandeep Patil" Date="2011-04-20" Remarks="First build." />
  <CompilerInfo header="package fb.rt.pnp;" />
  <InterfaceList>
    <EventInputs>
      <Event Name="INIT" Comment="Initialization Request">
        <With Var="Extend" />
      </Event>
      <Event Name="CLK" Comment="Clock signal">
        <With Var="Extend" />
      </Event>
      <Event Name="FAULT" />
      <Event Name="RESUME" />
    </EventInputs>
    <EventOutputs>
      <Event Name="INITO" Comment="Initialization Confirm">
        <With Var="C_FWD" />
        <With Var="C_BWD" />
      </Event>
      <Event Name="CHG" Comment="Data outputs changed">
        <With Var="C_FWD" />
        <With Var="C_BWD" />
      </Event>
      <Event Name="OFAULT" />
    </EventOutputs>
    <InputVars>
      <VarDeclaration Name="Extend" Type="BOOL" Comment="Move forwards" />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="C_FWD" Type="BOOL" Comment="Confirmed move forwards" />
      <VarDeclaration Name="C_BWD" Type="BOOL" Comment="Confirmed move backwards" />
    </OutputVars>
  </InterfaceList>
  <BasicFB>
    <ECC>
      <ECState Name="START" Comment="Initial State" x="1161.111" y="166.6667" />
      <ECState Name="INIT" Comment="Initialization" x="3222.222" y="166.6667">
        <ECAction Algorithm="INIT" Output="INITO" />
      </ECState>
      <ECState Name="MOVE_FWD" x="1690" y="1046.666">
        <ECAction Algorithm="FWD" Output="CHG" />
      </ECState>
      <ECState Name="MOVE_BWD" x="4540" y="1214.444">
        <ECAction Algorithm="BWD" Output="CHG" />
      </ECState>
      <ECState Name="STOP" x="3222.222" y="1050">
        <ECAction Algorithm="STOP" Output="CHG" />
      </ECState>
      <ECState Name="FAILURE" x="3164.444" y="2057.778">
        <ECAction Algorithm="STOP" Output="OFAULT" />
      </ECState>
      <ECTransition Source="START" Destination="INIT" Condition="INIT" x="2210.188" y="87.85161" />
      <ECTransition Source="INIT" Destination="MOVE_FWD" Condition="CLK AND Extend" x="2003.851" y="405.5104">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="523.7173,54.17641,410.8827,119.0597" />
      </ECTransition>
      <ECTransition Source="INIT" Destination="MOVE_BWD" Condition="CLK AND NOT Extend" x="4449.485" y="600.7915">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="1105.2,104.7724,1204.076,183.3902" />
      </ECTransition>
      <ECTransition Source="MOVE_FWD" Destination="FAILURE" Condition="FAULT" x="2243.214" y="1755.089">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="496.0179,416.6376,605.7426,491.9344" />
      </ECTransition>
      <ECTransition Source="STOP" Destination="FAILURE" Condition="FAULT" x="3617.873" y="1416.387">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="934.9583,305.5806,930.6421,380.865" />
      </ECTransition>
      <ECTransition Source="MOVE_BWD" Destination="FAILURE" Condition="FAULT" x="4247.045" y="1939.74">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="1138.4,476.652,1037.956,538.2328" />
      </ECTransition>
      <ECTransition Source="STOP" Destination="MOVE_FWD" Condition="CLK&amp;Extend" x="2496.972" y="1130.281">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="675.1539,287.2433,573.3629,287.0005" />
      </ECTransition>
      <ECTransition Source="STOP" Destination="MOVE_BWD" Condition="CLK AND NOT Extend" x="3866.86" y="655.8801">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="923.4167,123.3942,1007.227,133.8526" />
      </ECTransition>
      <ECTransition Source="FAILURE" Destination="STOP" Condition="RESUME" x="2953.546" y="1478.889">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="720.3794,401.1144,724.6956,325.83" />
      </ECTransition>
      <ECTransition Source="MOVE_FWD" Destination="MOVE_BWD" Condition="CLK AND NOT Extend" x="2958.483" y="734.1501">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="624.68,148.7389,829.988,160.9657" />
      </ECTransition>
      <ECTransition Source="MOVE_BWD" Destination="MOVE_FWD" Condition="CLK AND Extend" x="2787.224" y="1637.574">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="775.3846,453.3949,570.4547,441.198" />
      </ECTransition>
      <ECTransition Source="MOVE_FWD" Destination="MOVE_FWD" Condition="CLK AND Extend" x="1490.812" y="1417.822">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="370.9697,381.0367,345.9059,381.0367" />
      </ECTransition>
      <ECTransition Source="MOVE_BWD" Destination="MOVE_BWD" Condition="CLK AND NOT Extend" x="4352.79" y="1608.532">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="1087.194,430.6251,1062.131,430.6251" />
      </ECTransition>
      <ECTransition Source="INIT" Destination="FAILURE" Condition="FAULT" x="3283.452" y="1505.615">
        <Attribute Name="Configuration.Transaction.BezierPoints" Value="828.1844,334.7386,823.6354,483.6341" />
      </ECTransition>
    </ECC>
    <Algorithm Name="INIT" Comment="Initialization algorithm">
      <ST Text="C_FWD := FALSE;&#xD;&#xA;C_BWD := FALSE;" />
    </Algorithm>
    <Algorithm Name="FWD" Comment="Normally executed algorithm">
      <ST Text="C_FWD := TRUE;&#xD;&#xA;C_BWD := FALSE;" />
    </Algorithm>
    <Algorithm Name="BWD">
      <ST Text="C_FWD := FALSE;&#xD;&#xA;C_BWD := TRUE;" />
    </Algorithm>
    <Algorithm Name="STOP">
      <ST Text="C_FWD := FALSE;&#xD;&#xA;C_BWD := FALSE;" />
    </Algorithm>
  </BasicFB>
</FBType>