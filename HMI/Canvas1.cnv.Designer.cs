﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/24/2014
 * Time: 4:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for Canvas1.
	/// </summary>
	partial class Canvas1
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.View = new HMI.Main.Symbols.PnPView.sDefault();
			// 
			// View
			// 
			this.View.BeginInit();
			this.View.AngleIgnore = false;
			this.View.DesignTransformation = new NxtControl.Drawing.Matrix(1D, 0D, 0D, 1D, -1D, 0D);
			this.View.Name = "View";
			this.View.SecurityToken = ((uint)(4294967295u));
			this.View.TagName = "B084C1C70FB80D1A";
			this.View.EndInit();
			// 
			// Canvas1
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(0D)), ((float)(974D)), ((float)(709D)));
			this.Name = "Canvas1";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.View});
			this.Size = new System.Drawing.Size(974, 709);
		}
		private HMI.Main.Symbols.PnPView.sDefault View;
		#endregion
	}
}
