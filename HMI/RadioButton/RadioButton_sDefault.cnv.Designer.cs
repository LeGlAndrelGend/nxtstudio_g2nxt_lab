﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/28/2014
 * Time: 11:33 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.RadioButton
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.symbolPanel1 = new NxtControl.GuiFramework.SymbolPanel();
			// 
			// symbolPanel1
			// 
			this.symbolPanel1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel1.Location = new System.Drawing.Point(0, 3);
			this.symbolPanel1.Name = "symbolPanel1";
			this.symbolPanel1.Size = new System.Drawing.Size(211, 42);
			this.symbolPanel1.TabIndex = 0;
			this.symbolPanel1.SetHMISymbolType("HMI.Main.Symbols.RadioButton.RadioButtonGroup");
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.symbolPanel1});
			this.SymbolSize = new System.Drawing.Size(214, 46);
		}
		private NxtControl.GuiFramework.SymbolPanel symbolPanel1;
		#endregion
	}
}
