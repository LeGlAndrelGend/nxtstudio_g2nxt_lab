﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/28/2014
 * Time: 3:36 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.RadioButton
{
	/// <summary>
	/// Description of RadioButtonGroup.
	/// </summary>
	public partial class RadioButtonGroup : NxtControl.GuiFramework.HMISymbol
	{
		string lText;
	  string r1Text;
	  string r2Text;
		public RadioButtonGroup()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			this.REQ_Fired += new EventHandler<REQEventArgs>(onReqEvent);
		}
		void onReqEvent(object sender, REQEventArgs ea)
    { 
		  System.Windows.Forms.MessageBox.Show("REQ!");
		  ea.Get_LabelText(ref lText);
		  ea.Get_radio1Text(ref r1Text);
		  ea.Get_radio2Text(ref r2Text);
		  mLabel.Text = lText;
		  radioButton1.Text = r1Text;
		  radioButton2.Text = r2Text;
		}
	}
}
