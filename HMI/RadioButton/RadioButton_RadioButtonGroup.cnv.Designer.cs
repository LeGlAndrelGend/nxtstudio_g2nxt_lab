﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/28/2014
 * Time: 3:36 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.RadioButton
{
	/// <summary>
	/// Summary description for RadioButtonGroup.
	/// </summary>
	partial class RadioButtonGroup
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mLabel = new NxtControl.GuiFramework.FreeText();
			this.radioButton1 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton2 = new NxtControl.GuiFramework.RadioButton();
			// 
			// mLabel
			// 
			this.mLabel.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.mLabel.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.mLabel.Location = new NxtControl.Drawing.PointF(2, 9);
			this.mLabel.Name = "mLabel";
			this.mLabel.Text = "Label";
			// 
			// radioButton1
			// 
			this.radioButton1.Enabled = false;
			this.radioButton1.Location = new System.Drawing.Point(59, 6);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(64, 24);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.Text = "ON";
			// 
			// radioButton2
			// 
			this.radioButton2.Checked = true;
			this.radioButton2.Enabled = false;
			this.radioButton2.Location = new System.Drawing.Point(118, 6);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(64, 24);
			this.radioButton2.TabIndex = 1;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "OFF";
			// 
			// RadioButtonGroup
			// 
			this.Name = "RadioButtonGroup";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.mLabel,
									this.radioButton1,
									this.radioButton2});
			this.SymbolSize = new System.Drawing.Size(200, 35);
		}
		private NxtControl.GuiFramework.RadioButton radioButton2;
		private NxtControl.GuiFramework.RadioButton radioButton1;
		private NxtControl.GuiFramework.FreeText mLabel;
		#endregion
	}
}
