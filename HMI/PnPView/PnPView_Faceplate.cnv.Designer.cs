﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/5/2014
 * Time: 8:39 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using NxtControl.GuiFramework;

namespace HMI.Main.Faceplates.PnPView
{
	/// <summary>
	/// Summary description for Faceplate.
	/// </summary>
	partial class Faceplate
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.freeText2 = new NxtControl.GuiFramework.FreeText();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.line7 = new NxtControl.GuiFramework.Line();
			this.CYL3 = new NxtControl.GuiFramework.FreeText();
			this.CYL2 = new NxtControl.GuiFramework.FreeText();
			this.CYL1 = new NxtControl.GuiFramework.FreeText();
			this.line6 = new NxtControl.GuiFramework.Line();
			this.line2 = new NxtControl.GuiFramework.Line();
			this.line1 = new NxtControl.GuiFramework.Line();
			this.StatusOutline = new NxtControl.GuiFramework.Rectangle();
			this.line5 = new NxtControl.GuiFramework.Line();
			this.line4 = new NxtControl.GuiFramework.Line();
			this.line3 = new NxtControl.GuiFramework.Line();
			this.VCStatus = new NxtControl.GuiFramework.FreeText();
			this.tVCYL3 = new NxtControl.GuiFramework.FreeText();
			this.tVCYL2 = new NxtControl.GuiFramework.FreeText();
			this.tHCStatus = new NxtControl.GuiFramework.FreeText();
			this.tVCStatus = new NxtControl.GuiFramework.FreeText();
			this.HCStatus = new NxtControl.GuiFramework.FreeText();
			this.tVCYL1 = new NxtControl.GuiFramework.FreeText();
			this.VCYL3 = new NxtControl.GuiFramework.FreeText();
			this.VCYL2 = new NxtControl.GuiFramework.FreeText();
			this.VCYL1 = new NxtControl.GuiFramework.FreeText();
			this.tCYL3 = new NxtControl.GuiFramework.FreeText();
			this.tCYL2 = new NxtControl.GuiFramework.FreeText();
			this.tCYL1 = new NxtControl.GuiFramework.FreeText();
			this.group1 = new NxtControl.GuiFramework.Group();
			this.group3 = new NxtControl.GuiFramework.Group();
			this.tfVCYL3 = new NxtControl.GuiFramework.FreeText();
			this.tfVCYL2 = new NxtControl.GuiFramework.FreeText();
			this.tfVCYL1 = new NxtControl.GuiFramework.FreeText();
			this.toggleVCYL3 = new NxtControl.GuiFramework.TwoStateButton();
			this.toggleVCYL2 = new NxtControl.GuiFramework.TwoStateButton();
			this.toggleVCYL1 = new NxtControl.GuiFramework.TwoStateButton();
			this.tfCYL3 = new NxtControl.GuiFramework.FreeText();
			this.tfCYL2 = new NxtControl.GuiFramework.FreeText();
			this.tfCYL1 = new NxtControl.GuiFramework.FreeText();
			this.freeText3 = new NxtControl.GuiFramework.FreeText();
			this.toggleCYL3 = new NxtControl.GuiFramework.TwoStateButton();
			this.toggleCYL2 = new NxtControl.GuiFramework.TwoStateButton();
			this.toggleCYL1 = new NxtControl.GuiFramework.TwoStateButton();
			this.eStopButton = new NxtControl.GuiFramework.TwoStateButton();
			this.twoStateButton1 = new NxtControl.GuiFramework.TwoStateButton();
			this.freeText4 = new NxtControl.GuiFramework.FreeText();
			this.autoRadioButton = new NxtControl.GuiFramework.RadioButton();
			this.radioButton1 = new NxtControl.GuiFramework.RadioButton();
			this.freeText5 = new NxtControl.GuiFramework.FreeText();
			this.symbolPanel1 = new NxtControl.GuiFramework.SymbolPanel();
			this.symbolPanel2 = new NxtControl.GuiFramework.SymbolPanel();
			this.symbolPanel3 = new NxtControl.GuiFramework.SymbolPanel();
			this.symbolPanel4 = new NxtControl.GuiFramework.SymbolPanel();
			this.symbolPanel5 = new NxtControl.GuiFramework.SymbolPanel();
			this.symbolPanel6 = new NxtControl.GuiFramework.SymbolPanel();
			this.symbolPanel7 = new NxtControl.GuiFramework.SymbolPanel();
			// 
			// freeText2
			// 
			this.freeText2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText2.Font = new NxtControl.Drawing.Font("Courier New", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText2.Location = new NxtControl.Drawing.PointF(3, 185.05);
			this.freeText2.Name = "freeText2";
			this.freeText2.Text = "Scheduled?";
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText1.Location = new NxtControl.Drawing.PointF(4, 136.55);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Cylinders";
			// 
			// line7
			// 
			this.line7.EndPoint = new NxtControl.Drawing.PointF(337, 219);
			this.line7.Name = "line7";
			this.line7.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line7.StartPoint = new NxtControl.Drawing.PointF(337, 122);
			// 
			// CYL3
			// 
			this.CYL3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.CYL3.Font = new NxtControl.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
			this.CYL3.Location = new NxtControl.Drawing.PointF(194, 136.55);
			this.CYL3.Name = "CYL3";
			this.CYL3.Text = "CYL3";
			// 
			// CYL2
			// 
			this.CYL2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.CYL2.Font = new NxtControl.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
			this.CYL2.Location = new NxtControl.Drawing.PointF(146, 136.55);
			this.CYL2.Name = "CYL2";
			this.CYL2.Text = "CYL2";
			// 
			// CYL1
			// 
			this.CYL1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.CYL1.Font = new NxtControl.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
			this.CYL1.Location = new NxtControl.Drawing.PointF(94, 136.55);
			this.CYL1.Name = "CYL1";
			this.CYL1.Text = "CYL1";
			// 
			// line6
			// 
			this.line6.EndPoint = new NxtControl.Drawing.PointF(287, 219);
			this.line6.Name = "line6";
			this.line6.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line6.StartPoint = new NxtControl.Drawing.PointF(287, 122);
			// 
			// line2
			// 
			this.line2.EndPoint = new NxtControl.Drawing.PointF(387, 170.5);
			this.line2.Name = "line2";
			this.line2.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line2.StartPoint = new NxtControl.Drawing.PointF(3, 170.5);
			// 
			// line1
			// 
			this.line1.EndPoint = new NxtControl.Drawing.PointF(87, 219);
			this.line1.Name = "line1";
			this.line1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line1.StartPoint = new NxtControl.Drawing.PointF(87, 122);
			// 
			// StatusOutline
			// 
			this.StatusOutline.Bounds = new NxtControl.Drawing.RectF(((float)(3)), ((float)(122)), ((float)(384)), ((float)(97)));
			this.StatusOutline.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.StatusOutline.Name = "StatusOutline";
			this.StatusOutline.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// line5
			// 
			this.line5.EndPoint = new NxtControl.Drawing.PointF(237, 219);
			this.line5.Name = "line5";
			this.line5.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line5.StartPoint = new NxtControl.Drawing.PointF(237, 122);
			// 
			// line4
			// 
			this.line4.EndPoint = new NxtControl.Drawing.PointF(187, 219);
			this.line4.Name = "line4";
			this.line4.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line4.StartPoint = new NxtControl.Drawing.PointF(187, 122);
			// 
			// line3
			// 
			this.line3.EndPoint = new NxtControl.Drawing.PointF(137, 219);
			this.line3.Name = "line3";
			this.line3.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line3.StartPoint = new NxtControl.Drawing.PointF(137, 122);
			// 
			// VCStatus
			// 
			this.VCStatus.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.VCStatus.Font = new NxtControl.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold);
			this.VCStatus.Location = new NxtControl.Drawing.PointF(31, 71);
			this.VCStatus.Name = "VCStatus";
			this.VCStatus.Text = "Verticle Cylinder: ";
			// 
			// tVCYL3
			// 
			this.tVCYL3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tVCYL3.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tVCYL3.Location = new NxtControl.Drawing.PointF(343, 182.14000000000002);
			this.tVCYL3.Name = "tVCYL3";
			this.tVCYL3.Text = "NO";
			// 
			// tVCYL2
			// 
			this.tVCYL2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tVCYL2.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tVCYL2.Location = new NxtControl.Drawing.PointF(295, 182.14000000000002);
			this.tVCYL2.Name = "tVCYL2";
			this.tVCYL2.Text = "NO";
			// 
			// tHCStatus
			// 
			this.tHCStatus.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tHCStatus.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tHCStatus.Location = new NxtControl.Drawing.PointF(197, 90);
			this.tHCStatus.Name = "tHCStatus";
			this.tHCStatus.Text = "Not Scheduled";
			// 
			// tVCStatus
			// 
			this.tVCStatus.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tVCStatus.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tVCStatus.Location = new NxtControl.Drawing.PointF(197, 66);
			this.tVCStatus.Name = "tVCStatus";
			this.tVCStatus.Text = "Not Scheduled";
			// 
			// HCStatus
			// 
			this.HCStatus.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.HCStatus.Font = new NxtControl.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold);
			this.HCStatus.Location = new NxtControl.Drawing.PointF(31, 94);
			this.HCStatus.Name = "HCStatus";
			this.HCStatus.Text = "Horizontal Cylinder: ";
			// 
			// tVCYL1
			// 
			this.tVCYL1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tVCYL1.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tVCYL1.Location = new NxtControl.Drawing.PointF(243, 182.14000000000002);
			this.tVCYL1.Name = "tVCYL1";
			this.tVCYL1.Text = "NO";
			// 
			// VCYL3
			// 
			this.VCYL3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.VCYL3.Font = new NxtControl.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
			this.VCYL3.Location = new NxtControl.Drawing.PointF(345, 136.55);
			this.VCYL3.Name = "VCYL3";
			this.VCYL3.Text = "VCYL3";
			// 
			// VCYL2
			// 
			this.VCYL2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.VCYL2.Font = new NxtControl.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
			this.VCYL2.Location = new NxtControl.Drawing.PointF(297, 136.55);
			this.VCYL2.Name = "VCYL2";
			this.VCYL2.Text = "VCYL2";
			// 
			// VCYL1
			// 
			this.VCYL1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.VCYL1.Font = new NxtControl.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
			this.VCYL1.Location = new NxtControl.Drawing.PointF(245, 136.55);
			this.VCYL1.Name = "VCYL1";
			this.VCYL1.Text = "VCYL1";
			// 
			// tCYL3
			// 
			this.tCYL3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tCYL3.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tCYL3.Location = new NxtControl.Drawing.PointF(192, 182.14000000000002);
			this.tCYL3.Name = "tCYL3";
			this.tCYL3.Text = "NO";
			// 
			// tCYL2
			// 
			this.tCYL2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tCYL2.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tCYL2.Location = new NxtControl.Drawing.PointF(141, 182.14000000000002);
			this.tCYL2.Name = "tCYL2";
			this.tCYL2.Text = "NO";
			// 
			// tCYL1
			// 
			this.tCYL1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tCYL1.Font = new NxtControl.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold);
			this.tCYL1.Location = new NxtControl.Drawing.PointF(92, 182.14000000000002);
			this.tCYL1.Name = "tCYL1";
			this.tCYL1.Text = "NO";
			// 
			// group1
			// 
			this.group1.BeginInit();
			this.group1.Name = "group1";
			this.group1.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.StatusOutline,
									this.line1,
									this.line2,
									this.line3,
									this.line4,
									this.line5,
									this.line6,
									this.line7,
									this.freeText1,
									this.freeText2,
									this.CYL1,
									this.CYL2,
									this.CYL3,
									this.VCYL1,
									this.VCYL2,
									this.VCYL3,
									this.tCYL1,
									this.tCYL2,
									this.tCYL3,
									this.tVCYL1,
									this.tVCYL2,
									this.tVCYL3});
			this.group1.EndInit();
			// 
			// group3
			// 
			this.group3.BeginInit();
			this.group3.Name = "group3";
			this.group3.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.VCStatus,
									this.HCStatus,
									this.tVCStatus,
									this.tHCStatus});
			this.group3.EndInit();
			// 
			// tfVCYL3
			// 
			this.tfVCYL3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tfVCYL3.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.tfVCYL3.Location = new NxtControl.Drawing.PointF(119, 306);
			this.tfVCYL3.Name = "tfVCYL3";
			this.tfVCYL3.Text = "VCYL3";
			// 
			// tfVCYL2
			// 
			this.tfVCYL2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tfVCYL2.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.tfVCYL2.Location = new NxtControl.Drawing.PointF(119, 283);
			this.tfVCYL2.Name = "tfVCYL2";
			this.tfVCYL2.Text = "VCYL2";
			// 
			// tfVCYL1
			// 
			this.tfVCYL1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tfVCYL1.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.tfVCYL1.Location = new NxtControl.Drawing.PointF(119, 260);
			this.tfVCYL1.Name = "tfVCYL1";
			this.tfVCYL1.Text = "VCYL1";
			// 
			// toggleVCYL3
			// 
			this.toggleVCYL3.Bounds = new NxtControl.Drawing.RectF(((float)(94)), ((float)(307)), ((float)(20)), ((float)(20)));
			this.toggleVCYL3.DisabledTrueImageStream = null;
			this.toggleVCYL3.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.toggleVCYL3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.toggleVCYL3.Name = "toggleVCYL3";
			this.toggleVCYL3.Radius = 20;
			this.toggleVCYL3.CheckedChanged += new System.EventHandler(this.ToggleVCYL3CheckedChanged);
			// 
			// toggleVCYL2
			// 
			this.toggleVCYL2.Bounds = new NxtControl.Drawing.RectF(((float)(94)), ((float)(281)), ((float)(20)), ((float)(20)));
			this.toggleVCYL2.DisabledTrueImageStream = null;
			this.toggleVCYL2.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.toggleVCYL2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.toggleVCYL2.Name = "toggleVCYL2";
			this.toggleVCYL2.Radius = 20;
			this.toggleVCYL2.CheckedChanged += new System.EventHandler(this.ToggleVCYL2CheckedChanged);
			// 
			// toggleVCYL1
			// 
			this.toggleVCYL1.Bounds = new NxtControl.Drawing.RectF(((float)(94)), ((float)(256)), ((float)(20)), ((float)(20)));
			this.toggleVCYL1.DisabledTrueImageStream = null;
			this.toggleVCYL1.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.toggleVCYL1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.toggleVCYL1.Name = "toggleVCYL1";
			this.toggleVCYL1.Radius = 20;
			this.toggleVCYL1.CheckedChanged += new System.EventHandler(this.ToggleVCYL1CheckedChanged);
			// 
			// tfCYL3
			// 
			this.tfCYL3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tfCYL3.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.tfCYL3.Location = new NxtControl.Drawing.PointF(42, 306);
			this.tfCYL3.Name = "tfCYL3";
			this.tfCYL3.Text = "CYL3";
			// 
			// tfCYL2
			// 
			this.tfCYL2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tfCYL2.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.tfCYL2.Location = new NxtControl.Drawing.PointF(42, 283);
			this.tfCYL2.Name = "tfCYL2";
			this.tfCYL2.Text = "CYL2";
			// 
			// tfCYL1
			// 
			this.tfCYL1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.tfCYL1.Font = new NxtControl.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
			this.tfCYL1.Location = new NxtControl.Drawing.PointF(42, 260);
			this.tfCYL1.Name = "tfCYL1";
			this.tfCYL1.Text = "CYL1";
			// 
			// freeText3
			// 
			this.freeText3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText3.Font = new NxtControl.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold);
			this.freeText3.Location = new NxtControl.Drawing.PointF(8, 226);
			this.freeText3.Name = "freeText3";
			this.freeText3.Text = "Toggle Fault";
			// 
			// toggleCYL3
			// 
			this.toggleCYL3.Bounds = new NxtControl.Drawing.RectF(((float)(17)), ((float)(308)), ((float)(20)), ((float)(20)));
			this.toggleCYL3.DisabledTrueImageStream = null;
			this.toggleCYL3.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.toggleCYL3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.toggleCYL3.Name = "toggleCYL3";
			this.toggleCYL3.Radius = 20;
			this.toggleCYL3.CheckedChanged += new System.EventHandler(this.ToggleCYL3CheckedChanged);
			// 
			// toggleCYL2
			// 
			this.toggleCYL2.Bounds = new NxtControl.Drawing.RectF(((float)(17)), ((float)(282)), ((float)(20)), ((float)(20)));
			this.toggleCYL2.DisabledTrueImageStream = null;
			this.toggleCYL2.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.toggleCYL2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.toggleCYL2.Name = "toggleCYL2";
			this.toggleCYL2.Radius = 20;
			this.toggleCYL2.CheckedChanged += new System.EventHandler(this.ToggleCYL2CheckedChanged);
			// 
			// toggleCYL1
			// 
			this.toggleCYL1.Bounds = new NxtControl.Drawing.RectF(((float)(17)), ((float)(257)), ((float)(20)), ((float)(20)));
			this.toggleCYL1.DisabledTrueImageStream = null;
			this.toggleCYL1.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.toggleCYL1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.toggleCYL1.Name = "toggleCYL1";
			this.toggleCYL1.Radius = 20;
			this.toggleCYL1.CheckedChanged += new System.EventHandler(this.ToggleCYL1CheckedChanged);
			// 
			// eStopButton
			// 
			this.eStopButton.Bounds = new NxtControl.Drawing.RectF(((float)(188)), ((float)(254)), ((float)(70)), ((float)(70)));
			this.eStopButton.DisabledTrueImageStream = null;
			this.eStopButton.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.eStopButton.FalseText = "Emergency Stop";
			this.eStopButton.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.eStopButton.Name = "eStopButton";
			this.eStopButton.Radius = 20;
			this.eStopButton.TrueText = "Emergency Release";
			// 
			// twoStateButton1
			// 
			this.twoStateButton1.Bounds = new NxtControl.Drawing.RectF(((float)(270)), ((float)(254)), ((float)(70)), ((float)(70)));
			this.twoStateButton1.DisabledTrueImageStream = null;
			this.twoStateButton1.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.twoStateButton1.FalseText = "Hand In";
			this.twoStateButton1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.twoStateButton1.Name = "twoStateButton1";
			this.twoStateButton1.Radius = 20;
			this.twoStateButton1.TrueText = "Hand Out";
			// 
			// freeText4
			// 
			this.freeText4.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText4.Font = new NxtControl.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText4.Location = new NxtControl.Drawing.PointF(31, 44);
			this.freeText4.Name = "freeText4";
			this.freeText4.Text = "Controller Mode: ";
			// 
			// autoRadioButton
			// 
			this.autoRadioButton.Checked = true;
			this.autoRadioButton.Location = new System.Drawing.Point(200, 41);
			this.autoRadioButton.Name = "autoRadioButton";
			this.autoRadioButton.Size = new System.Drawing.Size(74, 24);
			this.autoRadioButton.TabIndex = 0;
			this.autoRadioButton.TabStop = true;
			this.autoRadioButton.Text = "Automatic";
			this.autoRadioButton.CheckedChanged += new System.EventHandler(this.AutoRadioButtonCheckedChanged);
			// 
			// radioButton1
			// 
			this.radioButton1.Location = new System.Drawing.Point(292, 41);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(61, 24);
			this.radioButton1.TabIndex = 1;
			this.radioButton1.Text = "Manual";
			this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1CheckedChanged);
			// 
			// freeText5
			// 
			this.freeText5.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText5.Font = new NxtControl.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold);
			this.freeText5.Location = new NxtControl.Drawing.PointF(8, 339);
			this.freeText5.Name = "freeText5";
			this.freeText5.Text = "Manual Control";
			// 
			// symbolPanel1
			// 
			this.symbolPanel1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel1.Location = new System.Drawing.Point(7, 364);
			this.symbolPanel1.Name = "symbolPanel1";
			this.symbolPanel1.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel1.TabIndex = 2;
			this.symbolPanel1.SetHMISymbolType("HMI.Main.Symbols.PnPView.CYL1ManControl");
			// 
			// symbolPanel2
			// 
			this.symbolPanel2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel2.Location = new System.Drawing.Point(7, 390);
			this.symbolPanel2.Name = "symbolPanel2";
			this.symbolPanel2.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel2.TabIndex = 2;
			this.symbolPanel2.SetHMISymbolType("HMI.Main.Symbols.PnPView.CYL2ManControl");
			// 
			// symbolPanel3
			// 
			this.symbolPanel3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel3.Location = new System.Drawing.Point(7, 416);
			this.symbolPanel3.Name = "symbolPanel3";
			this.symbolPanel3.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel3.TabIndex = 2;
			this.symbolPanel3.SetHMISymbolType("HMI.Main.Symbols.PnPView.CYL3ManControl");
			// 
			// symbolPanel4
			// 
			this.symbolPanel4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel4.Location = new System.Drawing.Point(191, 363);
			this.symbolPanel4.Name = "symbolPanel4";
			this.symbolPanel4.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel4.TabIndex = 2;
			this.symbolPanel4.SetHMISymbolType("HMI.Main.Symbols.PnPView.VCYL1ManControl");
			// 
			// symbolPanel5
			// 
			this.symbolPanel5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel5.Location = new System.Drawing.Point(191, 389);
			this.symbolPanel5.Name = "symbolPanel5";
			this.symbolPanel5.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel5.TabIndex = 2;
			this.symbolPanel5.SetHMISymbolType("HMI.Main.Symbols.PnPView.VCYL2ManControl");
			// 
			// symbolPanel6
			// 
			this.symbolPanel6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel6.Location = new System.Drawing.Point(191, 415);
			this.symbolPanel6.Name = "symbolPanel6";
			this.symbolPanel6.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel6.TabIndex = 2;
			this.symbolPanel6.SetHMISymbolType("HMI.Main.Symbols.PnPView.VCYL3ManControl");
			// 
			// symbolPanel7
			// 
			this.symbolPanel7.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.symbolPanel7.Location = new System.Drawing.Point(93, 441);
			this.symbolPanel7.Name = "symbolPanel7";
			this.symbolPanel7.Size = new System.Drawing.Size(186, 37);
			this.symbolPanel7.TabIndex = 2;
			this.symbolPanel7.SetHMISymbolType("HMI.Main.Symbols.PnPView.VaccumManControl");
			// 
			// Faceplate
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(403)), ((float)(506)));
			this.Name = "Faceplate";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.group3,
									this.toggleCYL1,
									this.toggleCYL2,
									this.toggleCYL3,
									this.freeText3,
									this.tfCYL1,
									this.tfCYL2,
									this.tfCYL3,
									this.toggleVCYL1,
									this.toggleVCYL2,
									this.toggleVCYL3,
									this.tfVCYL1,
									this.tfVCYL2,
									this.tfVCYL3,
									this.group1,
									this.eStopButton,
									this.twoStateButton1,
									this.freeText4,
									this.freeText5,
									this.symbolPanel1,
									this.symbolPanel2,
									this.symbolPanel3,
									this.symbolPanel4,
									this.symbolPanel5,
									this.symbolPanel6,
									this.symbolPanel7,
									this.autoRadioButton,
									this.radioButton1});
			this.Size = new System.Drawing.Size(403, 506);
		}
		private NxtControl.GuiFramework.SymbolPanel symbolPanel7;
		private NxtControl.GuiFramework.SymbolPanel symbolPanel6;
		private NxtControl.GuiFramework.SymbolPanel symbolPanel5;
		private NxtControl.GuiFramework.SymbolPanel symbolPanel4;
		private NxtControl.GuiFramework.SymbolPanel symbolPanel3;
		private NxtControl.GuiFramework.SymbolPanel symbolPanel2;
		private NxtControl.GuiFramework.SymbolPanel symbolPanel1;
		private NxtControl.GuiFramework.FreeText freeText5;
		private NxtControl.GuiFramework.RadioButton radioButton1;
		private NxtControl.GuiFramework.RadioButton autoRadioButton;
		private NxtControl.GuiFramework.FreeText freeText4;
		private NxtControl.GuiFramework.TwoStateButton twoStateButton1;
		private NxtControl.GuiFramework.TwoStateButton eStopButton;
		private NxtControl.GuiFramework.FreeText tfVCYL3;
		private NxtControl.GuiFramework.FreeText tfVCYL2;
		private NxtControl.GuiFramework.FreeText tfVCYL1;
		private NxtControl.GuiFramework.TwoStateButton toggleVCYL3;
		private NxtControl.GuiFramework.TwoStateButton toggleVCYL2;
		private NxtControl.GuiFramework.TwoStateButton toggleVCYL1;
		private NxtControl.GuiFramework.FreeText tfCYL3;
		private NxtControl.GuiFramework.FreeText tfCYL2;
		private NxtControl.GuiFramework.FreeText tfCYL1;
		private NxtControl.GuiFramework.FreeText freeText3;
		private NxtControl.GuiFramework.TwoStateButton toggleCYL3;
		private NxtControl.GuiFramework.TwoStateButton toggleCYL2;
		private NxtControl.GuiFramework.TwoStateButton toggleCYL1;
		private NxtControl.GuiFramework.Group group3;
		private NxtControl.GuiFramework.Group group1;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.Line line7;
		private NxtControl.GuiFramework.Line line6;
		private NxtControl.GuiFramework.FreeText CYL1;
		private NxtControl.GuiFramework.FreeText freeText2;
		private NxtControl.GuiFramework.FreeText CYL2;
		private NxtControl.GuiFramework.Line line5;
		private NxtControl.GuiFramework.Line line1;
		private NxtControl.GuiFramework.Rectangle StatusOutline;
		private NxtControl.GuiFramework.FreeText CYL3;
		private NxtControl.GuiFramework.Line line4;
		private NxtControl.GuiFramework.Line line3;
		private NxtControl.GuiFramework.Line line2;
		private NxtControl.GuiFramework.FreeText tVCYL3;
		private NxtControl.GuiFramework.FreeText tVCYL2;
		private NxtControl.GuiFramework.FreeText tVCYL1;
		private NxtControl.GuiFramework.FreeText tVCStatus;
		private NxtControl.GuiFramework.FreeText HCStatus;
		private NxtControl.GuiFramework.FreeText VCStatus;
		private NxtControl.GuiFramework.FreeText tCYL3;
		private NxtControl.GuiFramework.FreeText VCYL2;
		private NxtControl.GuiFramework.FreeText VCYL1;
		private NxtControl.GuiFramework.FreeText tHCStatus;
		private NxtControl.GuiFramework.FreeText tCYL2;
		private NxtControl.GuiFramework.FreeText tCYL1;
		private NxtControl.GuiFramework.FreeText VCYL3;
		#endregion
	}
}
