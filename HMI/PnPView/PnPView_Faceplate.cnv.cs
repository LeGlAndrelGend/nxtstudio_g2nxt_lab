﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/5/2014
 * Time: 8:39 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;
using HMI.Main.Symbols.PnPView;

namespace HMI.Main.Faceplates.PnPView
{
	/// <summary>
	/// Description of Faceplate.
	/// </summary>
	public partial class Faceplate : NxtControl.GuiFramework.HMIFaceplate
	{
	  bool bCYL1;
	  bool bCYL2;
	  bool bCYL3;
	  bool bVCYL1;
	  bool bVCYL2;
	  bool bVCYL3;
	  bool fCYL1;
	  bool fCYL2;
	  bool fCYL3;
	  bool fVCYL1;
	  bool fVCYL2;
	  bool fVCYL3;
	  
		public Faceplate()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			this.SET1_Fired += new EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs>(onSet1);
			this.SET2_Fired += new EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs>(onSet2);
			this.SET3_Fired += new EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs>(onSet3);
			this.SET4_Fired += new EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs>(onSet4);
			this.SET5_Fired += new EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs>(onSet5);
			this.SET6_Fired += new EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs>(onSet6);
			this.Reset_Fired += new EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs>(onReset);			
			this.VerScheduled_Fired += new EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs>(onVerScheduled);
			this.HorScheduled_Fired += new EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs>(onHorScheduled);
		}
		void onHorScheduled(object sender, HMI.Main.Symbols.PnPView.HorScheduledEventArgs ea)
    {
		  tHCStatus.Text = "Scheduled";
		  tHCStatus.Color = NxtControl.Drawing.Color.GetNamedColor("green");
		  if(bCYL1)
		    tCYL1.Text = "YES";
		  else
		    tCYL1.Text = "NO";
		  if(bCYL2)
		    tCYL2.Text = "YES";
		  else
		    tCYL2.Text = "NO";
		  if(bCYL3)
		    tCYL3.Text = "YES";
		  else
		    tCYL3.Text = "NO";
		}
		void onVerScheduled(object sender, HMI.Main.Symbols.PnPView.VerScheduledEventArgs ea)
    {
		  tVCStatus.Text = "Scheduled";
		  tVCStatus.Color = NxtControl.Drawing.Color.GetNamedColor("green");
		  if(bVCYL1)
		    tVCYL1.Text = "YES";
		  else
		    tVCYL1.Text = "NO";
		  if(bVCYL2)
		    tVCYL2.Text = "YES";
		  else
		    tVCYL2.Text = "NO";
		  if(bVCYL3)
		    tVCYL3.Text = "YES";
		  else
		    tVCYL3.Text = "NO";
		}
		void onReset(object sender, HMI.Main.Symbols.PnPView.ResetEventArgs ea)
    {
		  tCYL1.Text = "NO";
		  tCYL2.Text = "NO";
		  tCYL3.Text = "NO";
		  tVCYL1.Text = "NO";
		  tVCYL2.Text = "NO";
		  tVCYL3.Text = "NO";
		  bCYL1 = false;
	    bCYL2 = false;
	    bCYL3 = false;
	    bVCYL1 = false;
	    bVCYL2 = false;
	    bVCYL3 = false;
		  tHCStatus.Text = "Not Scheduled";
		  tVCStatus.Text = "Not Scheduled";
		}
		void onSet1(object sender, HMI.Main.Symbols.PnPView.SET1EventArgs ea)
    {
		  bCYL1 = true;
		}
		void onSet2(object sender, HMI.Main.Symbols.PnPView.SET2EventArgs ea)
    {
		  bCYL2 = true;
		}
		void onSet3(object sender, HMI.Main.Symbols.PnPView.SET3EventArgs ea)
    {
		  bCYL3 = true;
		}
		void onSet4(object sender, HMI.Main.Symbols.PnPView.SET4EventArgs ea)
    {
		  bVCYL1 = true;
		}
		void onSet5(object sender, HMI.Main.Symbols.PnPView.SET5EventArgs ea)
    {
		  bVCYL2 = true;
		}
		void onSet6(object sender, HMI.Main.Symbols.PnPView.SET6EventArgs ea)
    {
		  bVCYL3 = true;
		}
		
		void ToggleCYL1CheckedChanged(object sender, EventArgs e)
		{
		  sendFault();
		}
		
		void sendFault()
		{
		  fCYL1 = toggleCYL1.Checked;
		  fCYL2 = toggleCYL2.Checked;
		  fCYL3 = toggleCYL3.Checked;
		  fVCYL1 = toggleVCYL1.Checked;
		  fVCYL2 = toggleVCYL2.Checked;
		  fVCYL3 = toggleVCYL3.Checked;
			this.FireEvent_FAULT(fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3);
		}
		
		void ToggleCYL2CheckedChanged(object sender, EventArgs e)
		{
			sendFault();
		}
		
		void ToggleCYL3CheckedChanged(object sender, EventArgs e)
		{
			sendFault();
		}
		
		void ToggleVCYL1CheckedChanged(object sender, EventArgs e)
		{
			sendFault();
		}
		
		void ToggleVCYL2CheckedChanged(object sender, EventArgs e)
		{
			sendFault();
		}
		
		void ToggleVCYL3CheckedChanged(object sender, EventArgs e)
		{
			sendFault();
		}
		
		void AutoRadioButtonCheckedChanged(object sender, EventArgs e)
		{
		  this.FireEvent_MODE(1);
		}
		
		void RadioButton1CheckedChanged(object sender, EventArgs e)
		{
			this.FireEvent_MODE(2);
		}
	}
}
